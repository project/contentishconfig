<?php

namespace Drupal\contentishconfig;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityTypeInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\StorageTransformEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ContentishConfigTransformationEventSubscriber implements EventSubscriberInterface {

  private StorageInterface $activeStorage;

  private EntityTypeManagerInterface $entityTypeManager;

  public function __construct(StorageInterface $activeStorage, EntityTypeManagerInterface $entityTypeManager) {
    $this->activeStorage = $activeStorage;
    $this->entityTypeManager = $entityTypeManager;
  }

  public static function getSubscribedEvents() {
    return [
      'config.transform.import' => ['onConfigTransformImport', -500],
      'config.transform.export' => ['onConfigTransformExport', 500],
    ];
  }

  public function onConfigTransformImport(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $storage->getAllCollectionNames()) as $collectionName) {
      $collectionStorage = $storage->createCollection($collectionName);
      $activeCollectionStorage = $this->activeStorage->createCollection($collectionName);
      foreach ($this->getContentishConfigNames() as $configName) {
        $data = $activeCollectionStorage->read($configName);
        if (is_array($data)) {
          $collectionStorage->write($configName, $data);
        }
      }
    }
  }

  public function onConfigTransformExport(StorageTransformEvent $event) {
    $storage = $event->getStorage();
    foreach (array_merge([StorageInterface::DEFAULT_COLLECTION], $storage->getAllCollectionNames()) as $collectionName) {
      $collection = $storage->createCollection($collectionName);
      foreach ($this->getContentishConfigNames() as $configName) {
        $collection->delete($configName);
      }
    }
  }

  protected function getContentishConfigNames() {
    $names = [];
    foreach ($this->entityTypeManager->getDefinitions() as $entityTypeId => $entityType) {
      if ($entityType instanceof ConfigEntityTypeInterface) {
        $storage = \Drupal::entityTypeManager()->getStorage($entityTypeId);
        assert ($storage instanceof ConfigEntityStorageInterface);
        foreach ($storage->loadMultiple() as $config) {
          assert($config instanceof ConfigEntityInterface);
          $contentishSetting = $config->getThirdPartySetting('contentishconfig', 'contentish');
          if (!isset($contentishSetting)) {
            $contentishSetting = self::contentishSelectionDefault($config);
          }
          if ($contentishSetting) {
            $names[] = $entityType->getConfigPrefix() . '.' . $config->id();
          }
        }
      }
    }
    return $names;
  }

  public static function contentishSelectionDefault(ConfigEntityInterface $config): bool {
    return strpos($config->id(), '_content_') === 0;
  }

}
