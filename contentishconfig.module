<?php

use Drupal\contentishconfig\ContentishConfigTransformationEventSubscriber;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Form\FormStateInterface;

function contentishconfig_form_alter(&$form, FormStateInterface $formState, $formId) {
  $formObject = $formState->getFormObject();
  if ($formObject instanceof EntityFormInterface && !$formObject instanceof EntityDeleteForm) {
    $config = $formObject->getEntity();
    if ($config instanceof ConfigEntityInterface) {
      $form['third_party_settings']['#tree'] = TRUE;
      $form['third_party_settings']['contentishconfig'] = [
        '#type' => 'fieldset',
        '#title' => t('Contentish config'),
      ];
      $form['third_party_settings']['contentishconfig']['contentish'] = [
        '#type' => 'checkbox',
        '#title' => t('Contentish'),
        '#description' => t('This config will be ignored for config-sync. Defaults to TRUE for config whose id starts with "_content_".'),
        '#default_value' => $config->getThirdPartySetting('contentishconfig', 'contentish',
          ContentishConfigTransformationEventSubscriber::contentishSelectionDefault($config)
        ),
      ];
      $form['#entity_builders'][] = 'contentishconfig_entity_builder';
    }
  }
}

function contentishconfig_entity_builder(string $entityType, ConfigEntityInterface $configEntity, &$form, FormStateInterface $formState) {
  $contentishSelection = $formState->getValue(['third_party_settings', 'contentishconfig', 'contentish'], NULL);
  $contentishSelection = isset($contentishSelection) ? (bool) $contentishSelection : $contentishSelection;
  $contentishSelectionDefault = ContentishConfigTransformationEventSubscriber::contentishSelectionDefault($configEntity);
  if ($contentishSelection !== $contentishSelectionDefault) {
    $configEntity->setThirdPartySetting('contentishconfig', 'contentish', $contentishSelection);
  }
  else {
    $configEntity->unsetThirdPartySetting('contentishconfig', 'contentish');
  }
}
